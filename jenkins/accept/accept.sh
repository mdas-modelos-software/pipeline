#!/bin/bash

echo "#######################"
echo "*** Acceptant  Test ***"
echo "#######################"

docker run --rm -v /root/.m2:/root/.m2 -v /root/mdas-modelos-software/pipeline/app:/app -w /app maven:3-alpine "$@"

