#!/bin/bash

echo "****************"
echo "* Building jar!*"
echo "****************"

PROJ=/root/mdas-modelos-software/pipeline/
cd /root/mdas-modelos-software/mdas_modelosdesarrollo
git pull origin master
cd /root/mdas-modelos-software/pipeline
cp -r /root/mdas-modelos-software/mdas_modelosdesarrollo $PROJ/app
docker run --rm -v /root/.m2:/root/.m2 -v $PROJ/app:/app -w /app maven:3-alpine "$@"
